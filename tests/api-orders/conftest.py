from copy import copy

import pytest
from requests import Session


@pytest.fixture(scope='session')
def ecommerce_admin_session():
    my_headers = {'admin': 'yes'}
    s = Session()
    s.headers = my_headers
    return s


@pytest.fixture
def global_session_with_additional_headers(global_session):
    local_session = copy(global_session)
    local_session.headers['pies'] = 'kot'
    return local_session
