class TestAnonymousOrdering:
    """Amonymous user ordering flow: """

    def test_successful_order_for_anonymous_user(self, global_session, new_order):
        """Successful ordering for user without account"""
        print('id in test 1', id(global_session))
        global_session.headers['du'] = 'pa'

    def test_unsuccessful_order_for_anonymous_user_with_cash(self, global_session, ecommerce_admin_session):
        """Failed ordering for user without account for cash payment"""
        print('id in test 2', id(global_session))
        print(id(ecommerce_admin_session))
        print(global_session.headers['du'])

    def test_update_order(self, new_order):
        print(new_order.id)

    def test_update_order_twice(self, new_order):
        print(new_order.id)
