import pytest
from requests import Session

from config import config
from move_to_library.orders import Order
from move_to_library.virtual_machines import VirtualMachine, AzureVirtualMachine, VMWareVirtualMachine


@pytest.fixture(scope='session')
def global_session():
    my_headers = {'foo': 'bar'}
    s = Session()
    s.headers = my_headers
    return s


@pytest.fixture
def new_order():
    o = Order()
    o.create()
    return o


def pytest_itemcollected(item):
    par = item.parent.obj
    node = item.obj
    pref = par.__doc__.strip() if par.__doc__ else par.__class__.__name__
    suf = node.__doc__.strip() if node.__doc__ else node.__name__
    if pref or suf:
        item._nodeid = ' '.join((pref, suf))


# Before / after all tests
@pytest.fixture(scope='session', autouse=True)
def setup_teardown_global():
    print('copying keys to register')
    yield
    print('Removing mapping in external resources')


# Before / after each test
@pytest.fixture(autouse=True)
def setup_teardown_per_test():
    print('preparing test')
    yield
    print('finalizing test')


@pytest.fixture
def new_virtual_machine():
    if config.environment_type =='AZURE':
        return AzureVirtualMachine(config.azure_url).provision()
    else:
        return VMWareVirtualMachine(config.vmware_url).provision()
