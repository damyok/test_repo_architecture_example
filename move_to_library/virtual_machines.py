class VirtualMachine:
    def __init__(self, url):
        self.url = url

    def provision(self):
        # if self.cloud == 'azure':
        #     self.url = 'azure://cokolwiek'
        #     print('New Azure VM provisioned!')
        #
        # else:
        #     self.url = 'vmware://cokolwiek'
        #     print('New VMWare machine provisioned!')
        # return self.url
        raise NotImplementedError()


class AzureVirtualMachine(VirtualMachine):
    def provision(self):
        pass


class VMWareVirtualMachine(VirtualMachine):
    def provision(self):
        pass


if __name__ == '__main__':
    avm = AzureVirtualMachine('http://localhost:1234')
    wmwvm = VMWareVirtualMachine('http://localhost:1234')
    avm.provision()
    wmwvm.provision()
